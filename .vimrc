execute pathogen#infect()
syntax on
filetype plugin indent on
map <C-n> :NERDTreeToggle<CR>
set noswapfile
set ruler 
set list listchars=tab:»·,trail:·
set number
set numberwidth=5
set laststatus=2
set textwidth=80
set colorcolumn=+1
set smartindent
set tabstop=2
set shiftwidth=2
set shiftround
set expandtab
syntax enable
set background=light
colorscheme jellybeans
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
autocmd vimenter * NERDTree

autocmd FileType css,scss,sass setlocal iskeyword+=-
" Get off my lawn
nnoremap <Left> :echoe "Use h"<CR>
nnoremap <Right> :echoe "Use l"<CR>
nnoremap <Up> :echoe "Use k"<CR>
nnoremap <Down> :echoe "Use j"<CR>
" Quicker window movement
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
let g:user_emmet_leader_key='<C-Z>'
hi treeDir guifg=#96CBFE guibg=#ccc
