source ~/.profile
alias mktgstg='bundle exec jekyll serve -w --config _config.yml,_config-local.yml,_config-development.yml'
alias mktglocal='bundle exec jekyll serve -w --config _config.yml,_config-local.yml'
alias mktgRAlocal='bundle exec jekyll serve -w --config _config.yml,_config-local.yml,_config-RA-local.yml'
alias mktgprod='bundle exec jekyll serve -w --config _config.yml'
alias bashreload="source ~/.bash_profile"
alias xbrowser="java -jar cbttunnel.jar -authkey u35ecba0d5aa9d20"
alias gco="git checkout"
alias gs="git status"
alias gc="git commit"
alias g="git"
alias ga="git add"
alias gitHelp="cat ~/git-reminder.md"
alias deployA="bundle exec cap alpha deploy"
alias deployB="bundle exec cap beta deploy"
alias deployP="bundle exec cap production deploy"
alias gvim='/Applications/MacVim.app/Contents/MacOS/Vim -g'
alias openMKT='cd ~/Sites/marketing-recurly/marketing/'
alias openApp='cd ~/Sites/recurly-app/'
alias openSnap='cd ~/Sites/snappy/'
alias simpleServer='python -m SimpleHTTPServer'
export PATH='/usr/local/sbin:$PATH'
export PATH=/usr/local/sbin:/Users/greg/.rbenv/bin:/Users/greg/.rbenv/shims:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin


export NVM_DIR="/Users/greg/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
