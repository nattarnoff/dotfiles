# Delete remote branch

$ git push origin :<BRANCH>

# Push a branch to remote with other name

$ git push origin <LOCAL>:<REMOTE>

# Checkout remote not on your local

$ git fetch origin && git checkout -b <LOCAL> origin/<REMOTE>

# Merge my file (good for images)

$ git checkout --ours <FILE>

# Merge remote file

$ git checkout --theirs <FILE>
