export PATH=$HOME/.rbenv/bin:$PATH
source /Applications/Xcode.app/Contents/Developer/usr/share/git-core/git-completion.bash
source /Applications/Xcode.app/Contents/Developer/usr/share/git-core/git-prompt.sh
GIT_PS1_SHOWDIRTYSTATE=true
export PS1='\[\033[32m\]\u: \[\033[34m\]\w\[\033[31m\]$(__git_ps1)\[\033[00m\]\$ '


### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"
